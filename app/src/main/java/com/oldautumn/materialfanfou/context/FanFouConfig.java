package com.oldautumn.materialfanfou.context;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class FanFouConfig {

    private Context mContext;
    private SharedPreferences mPreferences;
    private final static String PREFERENCE_APP_CONFIG = "app_config";
    private final static String KEY_PREFERENCE_APP_TOKEN = "token";
    private final static String KEY_PREFERENCE_APP_TOKEN_SECRET = "token_secret";
    private final static String KEY_PREFERENCE_CURRENT_USER_ID = "userId";
    private final static String KEY_PREFERENCE_CURRENT_USER_INFO = "userInfo";

    private volatile static FanFouConfig instance;

    private FanFouConfig(Context mContext){
        this.mContext = mContext;
        this.mPreferences = mContext.getSharedPreferences(PREFERENCE_APP_CONFIG,Context.MODE_PRIVATE);
    }

    public static FanFouConfig getInstance(Context context) {
        if (instance == null) {
            synchronized (FanFouConfig.class) {
                if (instance == null) {
                    instance = new FanFouConfig(context);
                }
            }
        }
        return instance;
    }



    public boolean saveApiAccessToken(String token, String tokenSecret){
        if (TextUtils.isEmpty(token) && TextUtils.isEmpty(tokenSecret)){
            return false;
        }
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(KEY_PREFERENCE_APP_TOKEN,token);
        editor.putString(KEY_PREFERENCE_APP_TOKEN_SECRET, tokenSecret);
        return editor.commit();
    }


    public boolean saveLoginedUser(String userId, String userInfo){
        if (TextUtils.isEmpty(userId) && TextUtils.isEmpty(userInfo)){
            return false;
        }
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(KEY_PREFERENCE_CURRENT_USER_ID,userId);
        editor.putString(KEY_PREFERENCE_CURRENT_USER_INFO, userInfo);
        return editor.commit();
    }


    public String getLoginedUserId(){
        return mPreferences.getString(KEY_PREFERENCE_CURRENT_USER_ID, "");
    }


    public boolean isLogin() {
        String token = mPreferences.getString(KEY_PREFERENCE_APP_TOKEN,null);
        String tokenSecret = mPreferences.getString(KEY_PREFERENCE_APP_TOKEN_SECRET,null);
        if (TextUtils.isEmpty(token) || TextUtils.isEmpty(tokenSecret)){
            return false;
        }
        return true;
    }

    public String getOauthToken() {
        return mPreferences.getString(KEY_PREFERENCE_APP_TOKEN,null);
    }


    public String getOauthTokenSecret() {
        return mPreferences.getString(KEY_PREFERENCE_APP_TOKEN_SECRET,null);
    }
}
