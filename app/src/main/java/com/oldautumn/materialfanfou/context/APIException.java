package com.oldautumn.materialfanfou.context;

public class APIException extends Exception {
    private ExceptionType exceptionType;
    public enum  ExceptionType{
        IO_ERROR,OAUTH_ERROR
    }
    public APIException(ExceptionType exceptionType){
        super();
        this.exceptionType = exceptionType;
    }

    public APIException(ExceptionType exceptionType,String errorMsg){
        super(errorMsg);
        this.exceptionType = exceptionType;
    }


    public APIException(ExceptionType exceptionType,String errorMsg,Throwable throwable){
        super(errorMsg,throwable);
        this.exceptionType = exceptionType;
    }

    public APIException(ExceptionType exceptionType,Throwable throwable){
        super(throwable);
        this.exceptionType = exceptionType;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("error type: ").append(exceptionType).append("error msg: ").append(getMessage()).toString();
    }
}
