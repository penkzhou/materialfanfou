package com.oldautumn.materialfanfou.model;

public class Status {
    public String created_at; //"created_at": "Wed Nov 09 07:15:21 +0000 2011",
    public String id;//"id": "UcIlC04F2pQ",
    public long rawid;//"rawid": 123456,
    public String text;//"text": "看看",
    public String source;//"source": "网页",
    public boolean truncated;//"truncated": false,
    public String in_reply_to_status_id;//"in_reply_to_status_id": "",
    public String in_reply_to_user_id;//"in_reply_to_user_id": "",
    public String in_reply_to_screen_name;// "in_reply_to_screen_name": "",
    public String repost_status_id;//"repost_status_id": "gcIghPhCxQ",
    public Status repost_status;//"repost_status": "转发的消息详情",
    public String repost_user_id;//"repost_user_id": "clock",
    public String repost_screen_name;//"repost_screen_name": "钟钟",
    public boolean favorited;//"favorited": false,
    public boolean is_self;//"favorited": false,
    public String location;
    public User user;
    public Photo photo;

}
