package com.oldautumn.materialfanfou.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class User {
    public String id;//"id": "superisaac",
    public String name;//        "name": "杲i杲",
    public String screen_name;//        "screen_name": "杲i杲",
    public String location; //       "location": "北京 朝阳区",
    public String gender;   //     "gender": "男",
    public String birthday;  //      "birthday": "",
    public String description;//         "description": "爱工作, 爱编程, 爱摄影, 爱暴走, 也爱哗哗的灌水. 我不是富二代, 我是技术宅男, 我要减肥.",
    public String profile_image_url;//         "profile_image_url": "http://avatar3.fanfou.com/s0/01/3a/ve.jpg?1304991298",
    public String profile_image_url_large;//         "profile_image_url_large": "http://avatar3.fanfou.com/l0/01/3a/ve.jpg?1304991298",
    public String url;////       "url": "",
    @Expose
    @SerializedName("protected")
    public boolean is_protected;//         "protected": false,
    public int followers_count;//         "followers_count": 1329,
    public int friends_count;//         "friends_count": 2011,
    public int favourites_count;//         "favourites_count": 29,
    public int statuses_count;//         "statuses_count": 5259,
    public boolean following;//         "following": true,
    public boolean notifications;//         "notifications": true,
    public String created_at;//         "created_at": "Thu Feb 24 10:01:24 +0000 2011",
    public int utc_offset;//         "utc_offset": 28800,
    public String profile_background_color;//         "profile_background_color": "#C4B9A3",
    public String profile_text_color;//         "profile_text_color": "#3C2215",
    public String profile_link_color;//         "profile_link_color": "#BC834A",
    public String profile_sidebar_fill_color;//         "profile_sidebar_fill_color": "#F0EADC",
    public String profile_sidebar_border_color;//         "profile_sidebar_border_color": "#FFFFFF",
    public String profile_background_image_url;//         "profile_background_image_url": "http://static.fanfou.com/img/bg/14.jpg",
    public String profile_background_tile;// v       "profile_background_tile": false

}
