package com.oldautumn.materialfanfou.network;

import android.content.Context;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;

import com.oldautumn.materialfanfou.context.FanFouConfig;
import com.oldautumn.materialfanfou.utils.Base64;
import com.oldautumn.materialfanfou.utils.Encoder;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okio.ByteString;

/**
 * Created by zhoupeng05 on 12/04/2017.
 * 用来作为授权后的请求拦截器
 * oauth_consumer_key	饭否应用的API Key
 * oauth_token	**Access Token**
 * oauth_signature_method	签名方法，目前只支持HMAC-SHA1
 * oauth_signature	签名值，签名方法见OAuthSignature
 * oauth_timestamp	时间戳，取当前时间
 * oauth_nonce	单次值，随机的字符串，防止重复请求
 */

public class AuthenticationInterceptor implements Interceptor {
    private Context context;


    private static final String UTF8 = "UTF-8";
    private static final String HMAC_SHA1 = "HmacSHA1";
    private static final String HMAC_SHA1_METHOD = "HMAC-SHA1";
    private static final String EMPTY_STRING = "";
    private static final String CARRIAGE_RETURN = "\r\n";

    private static final String OAUTH_HEADER = "OAuth ";

    public AuthenticationInterceptor(Context context) {
        this.context = context;
    }

    private static final String API_KEY = "b60fd7312b20f48850798a57fb6ad2d5";
    private static final String API_SECRET = "06237b00f79bb3fc9eae4c4aa8675eb3";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originRequest = chain.request();
        HttpUrl originHttpUrl = originRequest.url();
        Request.Builder newBuilder = originRequest.newBuilder();
        ArrayMap<String, String> arrayMap = new ArrayMap<>(6);
        arrayMap.put("oauth_consumer_key", API_KEY);
        String token = FanFouConfig.getInstance(context).getOauthToken();
        if (!TextUtils.isEmpty(token)) {
            arrayMap.put("oauth_token", token);
        }
        arrayMap.put("oauth_signature_method", HMAC_SHA1_METHOD);
        arrayMap.put("oauth_version", "1.0");
        arrayMap.put("oauth_timestamp", String.valueOf(new Timer().getMilis()));
        arrayMap.put("oauth_nonce", String.valueOf(new Timer().getRandomInteger() + new Timer().getMilis()));
        String queryString = originHttpUrl.query();
        String originUrl = originHttpUrl.url().toString();
        String encodeUrl = Encoder.encode(originUrl);
        if (!TextUtils.isEmpty(queryString)) {
            originUrl = originHttpUrl.toString().replace(originHttpUrl.query(), "");
            encodeUrl = Encoder.encode(originUrl.substring(0, originUrl.length()-1));
        }
        String signature = String.format("%s&%s&%s", originRequest.method(),encodeUrl , getSortedParameterEncodeValue(originHttpUrl, arrayMap));

        String keyString = Encoder.encode(API_SECRET) + '&';
        keyString += Encoder.encode(FanFouConfig.getInstance(context).getOauthTokenSecret());
        SecretKeySpec key = new SecretKeySpec((keyString).getBytes(UTF8),
                HMAC_SHA1);
        Mac mac = null;
        try {
            mac = Mac.getInstance(HMAC_SHA1);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            mac.init(key);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        byte[] bytes = mac.doFinal(signature.getBytes(UTF8));
        String realSign = Base64.encodeBytes(bytes)
                .replace(CARRIAGE_RETURN, EMPTY_STRING);
        arrayMap.put("oauth_signature", realSign);
        Log.e("api1", signature);
        StringBuilder authHeader = new StringBuilder(OAUTH_HEADER);
        for (Map.Entry<String, String> entry : arrayMap.entrySet()) {
            if (authHeader.length() > OAUTH_HEADER.length()) {
                authHeader.append(", ");
            }
            authHeader.append(String.format("%s=\"%s\"", entry.getKey(), entry.getValue()));
        }
        newBuilder.addHeader("Authorization", authHeader.toString());
        return chain.proceed(newBuilder.build());
    }

    private String getSortedParameterEncodeValue(HttpUrl httpUrl, ArrayMap<String, String> arrayMapParam) {
        Set<String> originParams = httpUrl.queryParameterNames();
        List<String> params = new ArrayList<>(originParams);
        try {
            params.addAll(arrayMapParam.keySet());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(params);
        if (params == null || params.size() == 0) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (String param : params) {
            try {
                String value;
                if (httpUrl.queryParameterNames().contains(param)) {
                    value = httpUrl.queryParameter(param);
                } else {
                    value = arrayMapParam.get(param);
                }

                builder.append('&').append(String.format("%s=%s", Encoder.encode(param), Encoder.encode(value)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        String tempResult = "";
        try {
            tempResult = Encoder.encode(builder.toString().substring(1));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tempResult;
    }

    static class Timer {
        private final Random rand = new Random();

        Long getMilis() {
            return System.currentTimeMillis() / 1000;
        }

        Integer getRandomInteger() {
            return rand.nextInt();
        }
    }
}
