package com.oldautumn.materialfanfou.network;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by zhoupeng05 on 12/04/2017.
 * 用来作为授权前（一般指登录的时候）的请求拦截器
 * oauth_consumer_key	饭否应用的API Key
 * oauth_token	**Access Token**
 * oauth_signature_method	签名方法，目前只支持HMAC-SHA1
 * oauth_signature	签名值，签名方法见OAuthSignature
 * oauth_timestamp	时间戳，取当前时间
 * oauth_nonce	单次值，随机的字符串，防止重复请求
 */

public class RequestAuthenticationInterceptor implements Interceptor {


    private static final String API_KEY = "b60fd7312b20f48850798a57fb6ad2d5";
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originRequest = chain.request();
        HttpUrl originUrl = originRequest.url();
        FormBody.Builder newFormBuilder = new FormBody.Builder();


        newFormBuilder.add("oauth_consumer_key", API_KEY);
        newFormBuilder.add("oauth_signature_method", "HMAC-SHA1");
        newFormBuilder.add("x_auth_mode","client_auth");
        newFormBuilder.add("oauth_timestamp", String.valueOf(System.currentTimeMillis()));
        newFormBuilder.add("oauth_nonce", String.valueOf(System.currentTimeMillis()));
        RequestBody requestBody = newFormBuilder.build();
        return chain.proceed(originRequest.newBuilder().post(requestBody).url(originUrl).build());
    }
}
