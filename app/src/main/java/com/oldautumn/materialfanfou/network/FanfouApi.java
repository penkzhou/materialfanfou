package com.oldautumn.materialfanfou.network;

import com.oldautumn.materialfanfou.model.Status;
import com.oldautumn.materialfanfou.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by zhoupeng05 on 11/04/2017.
 */

public interface FanfouApi {


    String API_HOST = "http://api.fanfou.com";
    String API_KEY = "b60fd7312b20f48850798a57fb6ad2d5";
    String API_SECRET = "06237b00f79bb3fc9eae4c4aa8675eb3";
    String CALLBACK_URL = "http://m.fanfou.com";


    String AUTHORIZE_URL = "http://fanfou.com/oauth/authorize?oauth_token=%s";
    String REQUEST_TOKEN_URL = "http://fanfou.com/oauth/request_token";
    String ACCESS_TOKEN_URL = "http://fanfou.com/oauth/access_token";


    String X_AUTH_USERNAME = "x_auth_username";
    String X_AUTH_PASSWORD = "x_auth_password";
    String X_AUTH_MODE = "x_auth_mode";

    String BASE_AUTHORIZE_URL = "http://fanfou.com";

    @FormUrlEncoded
    @POST("/oauth/access_token")
    Call<String> login(@Field(X_AUTH_USERNAME) String username,
                       @Field(X_AUTH_PASSWORD) String password,
                       @Field(X_AUTH_MODE) String authMode,
                       @Field("oauth_signature_method") String method,
                       @Field("oauth_consumer_key") String consumerKey);




    @GET("/account/verify_credentials.json")
    Call<User> getCurrentUser( );

    @GET("/statuses/home_timeline.json")
    Call<List<Status>> getHomeTimeline(@Query("mode") String mode, @Query("count") int count, @Query("since_id") long sinceId);



    @GET("/users/show.json")
    Call<User> getUserProfile(@Query("id") String id);
}
