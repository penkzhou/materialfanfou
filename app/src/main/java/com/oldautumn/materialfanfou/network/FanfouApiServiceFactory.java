package com.oldautumn.materialfanfou.network;

import android.content.Context;

import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by penkzhou on 15/04/2017.
 */

public class FanfouApiServiceFactory {

    private static volatile OkHttpClient apiClient = null;
    private final static OkHttpClient OAUTH_CLIENT= new OkHttpClient();

    private static volatile FanfouApi normalApi = null;
    private static volatile FanfouApi oauthApi = null;

    private static OkHttpClient getApiClient(Context context) {
        if (apiClient == null) {
            synchronized (FanfouApiServiceFactory.class) {
                if (apiClient == null) {
                    apiClient = new OkHttpClient().newBuilder()
                            .addInterceptor(new HttpLoggingInterceptor())
                            .addInterceptor(new AuthenticationInterceptor(context))
                            .build();
                }
            }
        }
        return apiClient;
    }

    public static FanfouApi getOauthApi() {
        if (oauthApi == null) {
            synchronized (FanfouApiServiceFactory.class) {
                if (oauthApi == null) {
                    oauthApi = new Retrofit.Builder()
                            .baseUrl(FanfouApi.BASE_AUTHORIZE_URL)
                            .client(OAUTH_CLIENT)
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build()
                            .create(FanfouApi.class);
                }
            }
        }
        return oauthApi;
    }

    public static FanfouApi getNormalApi(Context context) {
        if (normalApi == null) {
            synchronized (FanfouApiServiceFactory.class) {
                if (normalApi == null) {
                    normalApi = new Retrofit.Builder()
                            .baseUrl(FanfouApi.API_HOST)
                            .client(getApiClient(context))
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setDateFormat("E MMM dd HH:mm:ss Z yyyy").create()))
                            .build()
                            .create(FanfouApi.class);
                }
            }
        }
        return normalApi;
    }

}
