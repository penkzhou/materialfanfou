package com.oldautumn.materialfanfou.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.oldautumn.materialfanfou.R;
import com.oldautumn.materialfanfou.model.Status;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StatusListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Status> statusList;
    private Context mContext;
    private static final String TIME_FORMAT = "E MMM dd HH:mm:ss Z yyyy";

    private final static int NORMAL_STATUS = 0;
    private final static int REPOST_STATUS = 1;

    public StatusListAdapter(Context mContext, List<Status> statusList) {
        this.statusList = statusList;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View v;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case REPOST_STATUS:
                v = inflater.inflate(R.layout.status_with_repost_item, parent, false);
                viewHolder = new RepostViewHolder(v);
                break;
            case NORMAL_STATUS:
            default:
                v = inflater.inflate(R.layout.status_item, parent, false);
                viewHolder = new NormalViewHolder(v);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Status status = statusList.get(position);
        switch (holder.getItemViewType()) {
            case REPOST_STATUS:
                RepostViewHolder repostHolder = (RepostViewHolder) holder;
                repostHolder.mName.setText(status.user.name);
                repostHolder.mSource.setText(parseSource(status.source));
                repostHolder.mText.setText(status.text);
                repostHolder.mCreateTime.setText(formatTimeString(status.created_at));
                Picasso.with(mContext).load(status.user.profile_image_url_large).into(repostHolder.mProfileImg);
                repostHolder.mRepostName.setText(status.repost_status.user.name);
                repostHolder.mRepostSource.setText(parseSource(status.repost_status.source));
                repostHolder.mRepostText.setText(status.repost_status.text);
                repostHolder.mRepostCreateTime.setText(formatTimeString(status.repost_status.created_at));
                Picasso.with(mContext).load(status.repost_status.user.profile_image_url_large).into(repostHolder.mRepostProfileImg);
                break;
            case NORMAL_STATUS:
            default:
                NormalViewHolder normalHolder = (NormalViewHolder) holder;
                normalHolder.mName.setText(status.user.name);
                normalHolder.mSource.setText(parseSource(status.source));
                normalHolder.mText.setText(status.text);
                normalHolder.mCreateTime.setText(formatTimeString(status.created_at));
                Picasso.with(mContext).load(status.user.profile_image_url_large).into(normalHolder.mProfileImg);
                break;
        }
    }

    private String parseSource(String input) {
        Matcher matcher = Pattern
                .compile("<a href.+blank\">(.+)</a>").matcher(input);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return input;
    }


    public static CharSequence formatTimeString(String timeString) {
        SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT, Locale.US);
        try {
            long time = sdf.parse(timeString).getTime();
            return DateUtils.getRelativeTimeSpanString(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    

    @Override
    public int getItemCount() {
        return statusList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (statusList.get(position).repost_status != null) {
            return REPOST_STATUS;
        } else {
            return NORMAL_STATUS;
        }
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(true);
    }

    public static class NormalViewHolder extends RecyclerView.ViewHolder {
        public TextView mName;
        public TextView mCreateTime;
        public TextView mSource;
        public TextView mText;
        public ImageView mProfileImg;

        public NormalViewHolder(View v) {
            super(v);
            mCreateTime = (TextView) v.findViewById(R.id.status_created_time);
            mName = (TextView) v.findViewById(R.id.profile_name);
            mText = (TextView) v.findViewById(R.id.status_text);
            mSource = (TextView) v.findViewById(R.id.status_source);
            mProfileImg = (ImageView) v.findViewById(R.id.profile_img);
        }
    }


    public static class RepostViewHolder extends RecyclerView.ViewHolder {
        public TextView mName;
        public TextView mCreateTime;
        public TextView mSource;
        public TextView mText;
        public ImageView mProfileImg;
        public RelativeLayout repostLayout;
        public TextView mRepostName;
        public TextView mRepostCreateTime;
        public TextView mRepostSource;
        public TextView mRepostText;
        public ImageView mRepostProfileImg;

        public RepostViewHolder(View v) {
            super(v);
            mCreateTime = (TextView) v.findViewById(R.id.status_created_time);
            mName = (TextView) v.findViewById(R.id.profile_name);
            mText = (TextView) v.findViewById(R.id.status_text);
            mSource = (TextView) v.findViewById(R.id.status_source);
            mProfileImg = (ImageView) v.findViewById(R.id.profile_img);
            repostLayout = (RelativeLayout) v.findViewById(R.id.repost_status_layout);
            mRepostCreateTime = (TextView) v.findViewById(R.id.repost_status_created_time);
            mRepostName = (TextView) v.findViewById(R.id.repost_profile_name);
            mRepostText = (TextView) v.findViewById(R.id.repost_status_text);
            mRepostSource = (TextView) v.findViewById(R.id.repost_status_source);
            mRepostProfileImg = (ImageView) v.findViewById(R.id.repost_profile_img);
        }
    }
}
