package com.oldautumn.materialfanfou.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.oldautumn.materialfanfou.R;
import com.oldautumn.materialfanfou.context.FanFouConfig;


public class StartActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_start);

        boolean isLogin = FanFouConfig.getInstance(this).isLogin();
        Intent startIntent;
        if (isLogin) {
            startIntent = new Intent(getApplicationContext(), Main2Activity.class);
        } else {
            startIntent = new Intent(this, LoginActivity.class);
        }
        startActivity(startIntent);
        finish();
    }


}
