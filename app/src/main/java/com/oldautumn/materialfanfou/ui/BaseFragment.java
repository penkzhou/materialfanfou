package com.oldautumn.materialfanfou.ui;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {
    private final static boolean DEBUG = true;
    private String TAG = this.getClass().getSimpleName();



    public BaseFragment() {
        // Required empty public constructor
        if (DEBUG){
            Log.d(TAG,"Constructor()");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (DEBUG){
            Log.d(TAG,"onViewCreated()");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DEBUG){
            Log.d(TAG,"onCreate()");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (DEBUG){
            Log.d(TAG,"onAttach()");
        }
    }

    @Override
    public void onInflate(Context context, AttributeSet attrs, Bundle savedInstanceState) {
        super.onInflate(context, attrs, savedInstanceState);
        if (DEBUG){
            Log.d(TAG,"onInflate()");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (DEBUG){
            Log.d(TAG,"onActivityCreated()");
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (DEBUG){
            Log.d(TAG,"onViewStateRestored()");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (DEBUG){
            Log.d(TAG,"onStart()");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DEBUG){
            Log.d(TAG,"onResume()");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (DEBUG){
            Log.d(TAG,"onSaveInstanceState()");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (DEBUG){
            Log.d(TAG,"onPause()");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (DEBUG){
            Log.d(TAG,"onStop()");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (DEBUG){
            Log.d(TAG,"onDestroyView()");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (DEBUG){
            Log.d(TAG,"onDestroy()");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (DEBUG){
            Log.d(TAG,"onDetach()");
        }
    }




}
