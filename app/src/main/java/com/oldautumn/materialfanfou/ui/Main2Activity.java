package com.oldautumn.materialfanfou.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;

import com.oldautumn.materialfanfou.R;

public class Main2Activity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private static final String TAG_MAIN = "main";
    private static final String TAG_MINE = "mine";
    private static final String TAG_MESSAGE = "message";

    private static final String[] TAG_ARRAYS = {TAG_MAIN, TAG_MESSAGE, TAG_MINE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
    }

    private void showFragmentByTag(String tag) {
        if (TextUtils.isEmpty(tag)) {
            return;
        }
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (fragment == null) {
            fragment = FragmentFactory.getFragmentByTag(tag);
            transaction.add(R.id.fragment_container, fragment, tag);
        }
        for (String tempTag : TAG_ARRAYS) {
            Fragment tempFragment = getSupportFragmentManager().findFragmentByTag(tempTag);
            if (tempFragment != null) {
                transaction.hide(tempFragment);
            }
        }
        transaction.show(fragment).commitAllowingStateLoss();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                showFragmentByTag(TAG_MAIN);
                return true;
            case R.id.navigation_dashboard:
                showFragmentByTag(TAG_MESSAGE);
                return true;
            case R.id.navigation_notifications:
                showFragmentByTag(TAG_MINE);
                return true;
        }
        return false;
    }

    static class FragmentFactory {
        public static Fragment getFragmentByTag(String tag) {
            switch (tag) {
                case TAG_MAIN:
                    return HomeStatusListFragment.newInstance();
                case TAG_MESSAGE:
                    return MessageFragment.newInstance("1", "2");
                case TAG_MINE:
                    return ProfileFragment.newInstance("");
            }
            return HomeStatusListFragment.newInstance();
        }
    }
}
