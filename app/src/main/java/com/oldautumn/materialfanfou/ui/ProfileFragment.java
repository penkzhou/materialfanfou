package com.oldautumn.materialfanfou.ui;


import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oldautumn.materialfanfou.R;
import com.oldautumn.materialfanfou.context.FanFouConfig;
import com.oldautumn.materialfanfou.model.User;
import com.oldautumn.materialfanfou.network.FanfouApiServiceFactory;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileFragment extends BaseFragment {

    private static final String ARG_USER_ID = "userId";

    private TextView userName;
    private String userId;
    private TextView userProfile;
    private TextView userStatusCount;
    private TextView userFollowerCount;
    private TextView userFriendCount;
    private ImageView userPhoto;


    public static ProfileFragment newInstance(String userId) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getString(ARG_USER_ID);
            if (TextUtils.isEmpty(userId)) {
                userId = FanFouConfig.getInstance(getContext()).getLoginedUserId();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, null);
        userProfile = (TextView) rootView.findViewById(R.id.userInfoDesc);
        userProfile.setText("loading...");
        userFollowerCount = (TextView) rootView.findViewById(R.id.followerCount);
        userName = (TextView) rootView.findViewById(R.id.userName);
        userFriendCount = (TextView) rootView.findViewById(R.id.followerCount);
        userStatusCount = (TextView) rootView.findViewById(R.id.followerCount);
        userPhoto = (ImageView) rootView.findViewById(R.id.profilePhoto);
        userFollowerCount = (TextView) rootView.findViewById(R.id.followerCount);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        requestData();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            requestData();
        }
    }

    public void requestData() {
        FanfouApiServiceFactory.getNormalApi(getContext()).getUserProfile(userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    userProfile.setText(user.description);
                    userName.setText(user.name);
                    userFollowerCount.setText(user.followers_count + "");
                    userFriendCount.setText(user.friends_count + "");
                    userStatusCount.setText(user.statuses_count + "");
                    Picasso.with(getContext()).load(user.profile_image_url).into(userPhoto);
                } else {
                    userProfile.setText("data parse error");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                userProfile.setText(t.getMessage());
            }
        });
    }
}
