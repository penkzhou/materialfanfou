package com.oldautumn.materialfanfou.ui;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oldautumn.materialfanfou.R;
import com.oldautumn.materialfanfou.adapter.StatusListAdapter;
import com.oldautumn.materialfanfou.model.Status;
import com.oldautumn.materialfanfou.network.AuthenticationInterceptor;
import com.oldautumn.materialfanfou.network.FanfouApi;
import com.oldautumn.materialfanfou.network.FanfouApiServiceFactory;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class HomeStatusListFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ProgressBar emptyProgressBar;
    private SwipeRefreshLayout refreshLayout;
    private List<Status> statusList;
    private int sinceId = 0;


    public static HomeStatusListFragment newInstance() {
        HomeStatusListFragment fragment = new HomeStatusListFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public HomeStatusListFragment() {
    }

    private void fetchStatus(long sinceId) {
        FanfouApiServiceFactory.getNormalApi(getContext()).getHomeTimeline("lite", 20, sinceId).enqueue(new Callback<List<Status>>() {
            @Override
            public void onResponse(Call<List<Status>> call, Response<List<Status>> response) {
                if (!response.isSuccessful()) {
                    String errorString = null;
                    try {
                        errorString = response.errorBody().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    RequestError requestError = new Gson().fromJson(errorString, RequestError.class);
                    int infoStartIndex = requestError.error.indexOf("basestring is ");
                    Log.e("api2", requestError.error.substrinMg(infoStartIndex + "basestring is ".length()));
                }
                statusList = response.body();
                refreshLayout.setRefreshing(false);
//                if (mStatusList != null && !mStatusList.isEmpty()) {
//                    mAdapter = new StatusListAdapter(getContext(), mStatusList);
//                    mRecyclerView.setAdapter(mAdapter);
//                }
                emptyProgressBar.setVisibility(View.GONE);
                refreshLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<List<Status>> call, Throwable t) {

            }
        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_homestatuslist_list, container, false);
        // Set the adapter
        emptyProgressBar = (ProgressBar) view.findViewById(R.id.empty_bar);
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refreshLayout);
        refreshLayout.setOnRefreshListener(this);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.statusList);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnScrollListener(new StatusScrollListener(this));
        mAdapter = new StatusListAdapter(getContext(), new ArrayList<Status>());
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fetchStatus(sinceId);
    }

    @Override
    public void onRefresh() {
        fetchStatus(sinceId);
    }

    private static class RequestError{
        final String error;
        final String request;

        public RequestError(String error, String request) {
            this.error = error;
            this.request = request;
        }
    }

    private static class StatusScrollListener extends OnScrollListener{
        private WeakReference<HomeStatusListFragment> fragmentWeakReference;
        public StatusScrollListener(HomeStatusListFragment homeStatusListFragment) {
            fragmentWeakReference = new WeakReference<HomeStatusListFragment>(homeStatusListFragment);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            HomeStatusListFragment fragment = fragmentWeakReference.get();
            if (fragment == null) {
                return;
            }

        }
    }

}
